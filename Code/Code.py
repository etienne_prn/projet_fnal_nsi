# -*- coding: utf-8 -*-

import pygame

pygame.init()

# generer la fenetre du jeu
pygame.display.set_caption("Projet final")
screen = pygame.display.set_mode((1080, 720))

# importation de l'arriere plan
background = pygame.image.load('bg.png')

# importer le bouton pour commencer la partie
play_button = pygame.image.load('start.png')
play_button = pygame.transform.scale(play_button, (400, 150))
play_button_rect = play_button.get_rect()
play_button_rect.x = screen.get_width() / 3.33
play_button_rect.y = screen.get_height() / 2

# Permet de jouer
jeux = True

while jeux:
    
    # Applique le fond d'écran
    screen.blit(background, (0, 0))
    
    # Applique le bouton
    screen.blit(play_button, (320, 363))
    
    # Permet de jouer de la musique en fond
    pygame.mixer.init()
    pygame.mixer.music.load("Sources/Musique.mp3")
    pygame.mixer.music.play(loops=0)
    
    pygame.display.flip()
    
    # si le joueur ferme la fenetre
    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()